package dockertest.DockerTest.repository;

import dockertest.DockerTest.domain.Student;
import org.springframework.data.jpa.repository.JpaRepository;

public interface StudentRepository extends JpaRepository<Student, Long> {
}
