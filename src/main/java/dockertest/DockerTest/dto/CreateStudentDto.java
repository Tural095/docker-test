package dockertest.DockerTest.dto;

import lombok.Data;

@Data
public class CreateStudentDto {

    private String name;

    private Long age;
}
