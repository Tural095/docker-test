package dockertest.DockerTest.dto;


import lombok.Data;

@Data
public class StudentDto {

    private Long id;

    private String name;

    private Long age;
}
