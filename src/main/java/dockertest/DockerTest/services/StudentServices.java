package dockertest.DockerTest.services;

import dockertest.DockerTest.dto.CreateStudentDto;
import dockertest.DockerTest.dto.StudentDto;


public interface StudentServices {

    StudentDto getStudent(Long id);

    StudentDto createStudent(CreateStudentDto studentDto);

    StudentDto updateStudent(Long id, CreateStudentDto studentDto);

    void deleteStudent(Long id);
}
