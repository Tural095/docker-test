package dockertest.DockerTest.controller;

import dockertest.DockerTest.dto.CreateStudentDto;
import dockertest.DockerTest.dto.StudentDto;
import dockertest.DockerTest.services.StudentServices;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

@RestController
@RequiredArgsConstructor
@RequestMapping("/students")
public class StudentController {
    private final StudentServices studentServices;

    @GetMapping("/{id}")
    public StudentDto getStudent(@PathVariable Long id){
        return studentServices.getStudent(id);
    }

    @PostMapping
    public StudentDto createStudent(@RequestBody CreateStudentDto createStudentDto){
        return studentServices.createStudent(createStudentDto);
    }

    @PutMapping("/{id}")
    public StudentDto updateStudent(@PathVariable Long id, @RequestBody CreateStudentDto createStudentDto){
        return studentServices.updateStudent(id, createStudentDto);
    }

    @DeleteMapping("/{id}")
    public void deleteStudent(@PathVariable Long id){
        studentServices.deleteStudent(id);
    }
}
