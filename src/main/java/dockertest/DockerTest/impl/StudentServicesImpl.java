package dockertest.DockerTest.impl;

import dockertest.DockerTest.domain.Student;
import dockertest.DockerTest.dto.CreateStudentDto;
import dockertest.DockerTest.dto.StudentDto;
import dockertest.DockerTest.repository.StudentRepository;
import dockertest.DockerTest.services.StudentServices;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class StudentServicesImpl implements StudentServices {

    private final StudentRepository studentRepository;
    private final ModelMapper modelMapper;

    @Override
    public StudentDto getStudent(Long id) {
    Student student = studentRepository.findById(id).orElseThrow(()->new RuntimeException("Student not found"));
    return modelMapper.map(student, StudentDto.class);
    }

    @Override
    public StudentDto createStudent(CreateStudentDto studentDto) {
        Student student = studentRepository.save(modelMapper.map(studentDto, Student.class));
        return modelMapper.map(student, StudentDto.class);
    }

    @Override
    public StudentDto updateStudent(Long id, CreateStudentDto studentDto) {
        studentRepository.findById(id).orElseThrow(()->new RuntimeException("Student not found"));
        Student student=modelMapper.map(studentDto,Student.class);
        student.setId(id);
        return modelMapper.map(student, StudentDto.class);
    }

    @Override
    public void deleteStudent(Long id) {
        studentRepository.deleteById(id);

    }
}
